<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	 public function __construct()
	 {
	        parent::__construct();
		  $this->load->helper('url');
	      $this->load->database();
	      $this->load->model('admin_model');
	      $this->load->library('session');
		 if($this->session->userdata('type') == 'user'){
			 redirect('/');
		 }
	  }


	public function index()
	{
    if($this->session->userdata('email')){
        redirect('index.php/dashboard');
      }
      else
      {
        $this->load->view('admin/login');
      }
	}

  public function login()
  {
    if(!$this->session->userdata('email')){
        redirect('/admin');
      }
      else
      {
        $this->load->view('admin/layouts/header');
        $this->load->view('admin/layouts/side-bar');
				$data['final_data']= $this->admin_model->list();
				$data['final']= $this->admin_model->users();
				$data['total']= $this->admin_model->get_contactus();
				$data['totalCard']= $this->admin_model->get_totalCard();
				$data['totalCoupon']= $this->admin_model->get_totalCoupon();
        $this->load->view('admin/admin_dashboard', $data);
        $this->load->view('admin/layouts/footer');
      }


  }

  public function user_login()
  {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run())
        {
             //true
             $email = $this->input->post('email');
             $password = md5($this->input->post('password'));
             //model function
             if($this->admin_model->can_login($email, $password))
             {
               $res = $this->admin_model->getall($email);
                  // echo '<pre>';
                  // print_r($res);
                  // echo '</pre>';
                  // die();

                  $session_data = array(
                  	   'type' => "admin",
                       'email'     =>     $email,
                       'id'   => $res->id,
                       'username'   => $res->username,
                       'password'   => $res->password,

                  );


                  $this->session->set_userdata($session_data);
                 // redirect(base_url() . 'main/enter');
                   //redirect('login_controller/profile');
                   redirect('dashboard');
                   //echo "login ok";
             }
                 else
                 {
                      $this->session->set_flashdata('error', 'Invalid Username and Password please enter a valid details');
                      redirect('/admin');
                 }
              }else
              {
                   echo "error";
              }
    }

    public function logout()
    {
         $this->session->unset_userdata('email');
         $this->session->sess_destroy();
         redirect('/admin');
    }


		public function page($id = 0)
		{
			if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{
					if(!$id == 0){

						$this->load->view('admin/layouts/header');
						$this->load->view('admin/layouts/side-bar');
						$data['result']= $this->admin_model->fetch_page_by_id($id);
						$this->load->view('admin/add_pages', $data);
						$this->load->view('admin/layouts/footer');

					}else{
								$this->load->view('admin/layouts/header');
								$this->load->view('admin/layouts/side-bar');
								$data['result']= $this->admin_model->fetch_page_by_id($id);
								$this->load->view('admin/add_pages', $data);
								// $this->load->view('admin/add_pages');
								$this->load->view('admin/layouts/footer');
			       	}

				}


		}

		public function add_pages($id = 0){

				$this->load->library('form_validation');
				$this->form_validation->set_rules('title', 'Title', 'required');
				$this->form_validation->set_rules('content', 'Content', 'required');
					if($this->form_validation->run())
					{
						$slug = url_title($this->input->post('title'));
						$slug = strtolower($slug);

						 $this->db->select('*');
						 $this->db->from('pages');
						 $this->db->where('slug', $slug);
						 if($id != 0){
							 $this->db->where('id!= ',$id);
						 }
						 $query = $this->db->get();
						 $res =  $query->row_array();

						 $data = array(
														'title' => $this->input->post('title'),
														'slug' => $slug,
														'content' => $this->input->post('content')
												);

						 if(empty($res)){
							//	echo '<pre>';print_r($data); die();
                if($id == 0){

								$this->db->insert('pages', $data);
								$this->session->set_flashdata('success', 'Page created successfully');
								redirect('/pages');
							}else{

									$this->db->where('id', $id);
	                                $this->db->update('pages', $data);
									$this->session->set_flashdata('success', 'Page updated successfully');
									redirect('/pages');
	              }

							}else{

                               $this->session->set_userdata($data);
								$this->session->set_flashdata('error', 'Page title can not be same ');
								redirect('/add-page');
							}

					}
					else{
						echo "error";
					}


		}

		public function list_page(){
			if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{
					$this->load->view('admin/layouts/header');
					$this->load->view('admin/layouts/side-bar');
					$data['final_data']= $this->admin_model->list();
					$this->load->view('admin/list_page', $data);
					$this->load->view('admin/layouts/footer');

				}

		}

		public function delete_page($id){

			$this->admin_model->delete_data($id);
            $this->session->set_flashdata('success', 'Page Deleted successfully');
			$data['final_data']= $this->admin_model->list();
			redirect('/pages');


		}

		public function list_users(){
			if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{
					$this->load->view('admin/layouts/header');
					$this->load->view('admin/layouts/side-bar');
					$data['final']= $this->admin_model->users();
					$this->load->view('admin/list_users', $data);
					$this->load->view('admin/layouts/footer');

				}

		}

		public function list_contact_us(){
			if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{
					$this->load->view('admin/layouts/header');
					$this->load->view('admin/layouts/side-bar');
					$data['total']= $this->admin_model->get_contactus();
					$this->load->view('admin/list_contact_us', $data);
					$this->load->view('admin/layouts/footer');

				}

		}

		public function get_users(){

		     $this->db->select('*');
		     $this->db->from('clients_web');
		     $query=$this->db->get();
		     $final_data = $query->result_array();
		     echo '<pre>'; print_r($final_data); die();

		}

		public function delete_users($id){

				$this->admin_model->delete_user_data($id);
	            $this->session->set_flashdata('success', 'Page Deleted successfully');
				$data['final_data']= $this->admin_model->list();
				redirect('/users');


		  }

		 public function delete_contactus($id){

				$this->admin_model->delete_contact_data($id);
	            $this->session->set_flashdata('success', 'Page Deleted successfully');
				$data['total']= $this->admin_model->get_contactus();
				redirect('/qutoes');


		  }

  public function forgot_password(){

  	$this->load->view('admin/reset_password');

  }

	 public function forgotPassword(){

	  	$email = $this->input->post('email');

     // echo '<pre>';
     // print_r($email); die();

	  	$find_email = $this->admin_model->ForgotPassword($email);

	  	if(!$find_email){

	  		  $this->session->set_flashdata('msg',' Email not found!');
		      redirect('forgot-password');
	          // redirect(base_url().'user/Login','refresh');
	  	}else{

	             // echo "found";
	  		 $this->sendpassword($find_email);

	  		//      echo '<pre>';
     // print_r($data); die();



	  	}




  }

  public function sendpassword($data){

  	$email = $data['email'];
  	$sql = $this->db->query("SELECT *  from admin where email = '".$email."' ");
        $row=$sql->result_array();
        if ($sql->num_rows()>0){

	        $passwordplain = "";
	        $passwordplain  = rand(999999999,9999999999);
	        $newpass['password'] = md5($passwordplain);
	        $this->db->where('email', $email);
	        $this->db->update('admin', $newpass);
	        $mail_message='Dear '.$row[0]['username'].','. "\r\n";
	        $mail_message.='Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>'.$passwordplain.'</b>'."\r\n";
	        $mail_message.='<br>Please Update your password.';
	        $mail_message.='<br>Thanks & Regards';
	        $mail_message.='<br>Your company name';

                 $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
                );



			    $this->load->library('email', $config);

				$this->email->set_newline("\r\n");

				//Add file directory if you need to attach a file
				//$this->email->attach($file_dir_name);

				$this->email->from('Admin@site.com', 'admin');
				$this->email->to($email);

				$this->email->subject('From Cardcaddy');
				$this->email->message($mail_message);

				if($this->email->send()){
				   //Success email Sent
				   $this->session->set_flashdata('success', 'A new password willl be send to your email id please check.');
				   redirect('forgot-password');
				}else{
				   //Email Failed To Send
				   echo "<script>alert('Failed to send password, please try again!')</script>";
				}




        }else{
        	echo "error";
        }

  }

  		public function get_admin(){

		     $this->db->select('*');
		     $this->db->from('admin');
		     $query=$this->db->get();
		     $final_data = $query->result_array();
		     echo '<pre>'; print_r($final_data); die();

		}



		public function gift_card(){

						if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{

					$this->load->view('admin/layouts/header');
					$this->load->view('admin/layouts/side-bar');
					$this->load->view('admin/add_giftcard');
					$this->load->view('admin/layouts/footer');
				}

		}


				public function add_card($id = 0){


			    $config['upload_path']          = './assets/admin/images/';
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|txt|csv|xml|mp3|mp4';
                $config['overwrite']            = TRUE;
                $config['max_size']             = 2048000; // Can be set to particular file size , here it is 2 MB(2048 Kb)
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $new_name = time().$_FILES["image"]['name'];
				$config['file_name'] = $new_name;

                $this->load->library('upload', $config);

              if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        // echo '<pre>';
                        // print_r($error);
                        // die;

                        // $this->load->view('manage_profile', $error);
                	   $this->session->set_flashdata('error', $error);
                }else{

                	$image_data = $this->upload->data();
                   // echo '<pre>';print_r($image_data); die();

					$this->load->library('form_validation');
					$this->form_validation->set_rules('title', 'Title', 'required');
					$this->form_validation->set_rules('discount_per', 'Discount', 'required');
					if($this->form_validation->run())
					{

						 $data = array(
										'title' => $this->input->post('title'),
										'image'=>$new_name,
										'discount_per' => $this->input->post('discount_per'),
										"created" => date('Y-m-d H:i:s')
								 	 );

							//	echo '<pre>';print_r($data); die();
                          if($id == 0){

								$this->db->insert('gift_card', $data);
								$this->session->set_flashdata('success', 'gift card created successfully');
								redirect('giftcard');
							}else{

									$this->db->where('id', $id);
	                                $this->db->update('gift_card', $data);
									$this->session->set_flashdata('success', 'gift card updated successfully');
									redirect('/pages');
	                                }

							}
							else{
								echo "error";
							}


		}
	}

    public function get_attachments(){
      if(!$this->session->userdata('email')){
          redirect('/admin');
        }
        else
        {
          $this->load->view('admin/layouts/header');
          $this->load->view('admin/layouts/side-bar');
          $data['final']= $this->getAllUpload();
          $this->load->view('admin/list_media', $data);
          $this->load->view('admin/layouts/footer');

        }
    }
    public function getUserInfoById($id) {
      $this->db->select( '*' );
      $this->db->where( 'id', $id );
      $q = $this->db->get( 'clients_web' );
      return $q->row();
    }
    public function getAllUpload() {
     $this->db->select('*');
     $this->db->from('user_upload');
     $this->db->order_by("id", "desc");
     $query=$this->db->get();
     $attachments =  $query->result_array();
     $r = [];
     foreach ($attachments as $value) {
       $id = explode(',', $value['attachments']);
       $this->db->select('*');
       $this->db->from('attachments');
       $this->db->where_in('id', $id);
       $value['file_list'] = $this->db->get();
       $value['file_list'] = $value['file_list']->result_array();
       $value['user_details'] = $this->getUserInfoById($value['clients_web_id']);
       $r[] = $value;
     }
     // echo json_encode();
     // echo json_encode($r);
     return $r;
    }

    public function get_card(){

			if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{
					$this->load->view('admin/layouts/header');
					$this->load->view('admin/layouts/side-bar');
					$data['final']= $this->admin_model->get_card();
					$this->load->view('admin/list_giftcard', $data);
					$this->load->view('admin/layouts/footer');

				}
    }

		public function delete_gift_card($id){

			$this->admin_model->delete_giftcard_data($id);
            $this->session->set_flashdata('success', 'Gift card Deleted successfully');
			$data['final']= $this->admin_model->get_card();
			redirect('/giftcard');


		}



		public function update_giftcard($id){

			if($_FILES["image"]['name']!=""){


      			$config['upload_path']          = './assets/admin/images/';
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|txt|csv|xml|mp3|mp4';
                $config['overwrite']            = TRUE;
                $config['max_size']             = 2048000; // Can be set to particular file size , here it is 2 MB(2048 Kb)
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $new_name = time().$_FILES["image"]['name'];
				$config['file_name'] = $new_name;

                $this->load->library('upload', $config);

              if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        // echo '<pre>';
                        // print_r($error);
                        // die;

                        // $this->load->view('manage_profile', $error);
                	   $this->session->set_flashdata('error', $error);
                }else{

                	$image_data = $this->upload->data();
                   // echo '<pre>';print_r($image_data); die();

					$this->load->library('form_validation');
					$this->form_validation->set_rules('title', 'Title', 'required');
					$this->form_validation->set_rules('discount_per', 'Discount', 'required');
					if($this->form_validation->run())
					{

						 $data = array(
										'title' => $this->input->post('title'),
										'image'=>$new_name,
										'discount_per' => $this->input->post('discount_per'),
										"created" => date('Y-m-d H:i:s')
								 	 );

									$this->db->where('id', $id);
	                                $this->db->update('gift_card', $data);
									$this->session->set_flashdata('success', 'gift card updated successfully');
									redirect('/giftcard');

							}
							else{
								echo "error";
							}


		}

		}else{
			                $new_name=$this->input->post('old');
									 $data = array(
										'title' => $this->input->post('title'),
										'image'=>$new_name,
										'discount_per' => $this->input->post('discount_per'),
										"created" => date('Y-m-d H:i:s')
								 	 );

									$this->db->where('id', $id);
	                                $this->db->update('gift_card', $data);
									$this->session->set_flashdata('success', 'gift card updated successfully');
									redirect('/giftcard');


		}

}
	  public function update_giftcard_page($id)
		    {
			if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{

								$this->load->view('admin/layouts/header');
								$this->load->view('admin/layouts/side-bar');
								$data['result']= $this->admin_model->fetch_giftcard_by_id($id);
								$this->load->view('admin/update_giftcard', $data);
								// $this->load->view('admin/add_pages');
								$this->load->view('admin/layouts/footer');
			       	}




	}



  		public function view_giftcard_coupon(){

						if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{

					$this->load->view('admin/layouts/header');
					$this->load->view('admin/layouts/side-bar');
					$data['final']= $this->admin_model->get_card();
					$this->load->view('admin/add_giftcard_coupon', $data);
					$this->load->view('admin/layouts/footer');
				}

		}


	    public function add_coupon($id = 0){


			    $config['upload_path']          = './assets/admin/images/';
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|txt|csv|xml|mp3|mp4';
                $config['overwrite']            = TRUE;
                $config['max_size']             = 2048000; // Can be set to particular file size , here it is 2 MB(2048 Kb)
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $new_name = time().$_FILES["image"]['name'];
				$config['file_name'] = $new_name;

                $this->load->library('upload', $config);

              if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        // echo '<pre>';
                        // print_r($error);
                        // die;

                        // $this->load->view('manage_profile', $error);
                	   $this->session->set_flashdata('error', $error);
                }else{

                	$image_data = $this->upload->data();
                   // echo '<pre>';print_r($image_data); die();

					$this->load->library('form_validation');
					$this->form_validation->set_rules('gift_id', 'Gift card', 'required');
					$this->form_validation->set_rules('type', 'Type', 'required');
					$this->form_validation->set_rules('spend_policy', 'Spend policy', 'required');
					$this->form_validation->set_rules('value', 'Value', 'required');

					$this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric');
					$this->form_validation->set_rules('payment_opt1', 'Payment option 1', 'required');
					$this->form_validation->set_rules('payment_opt2', 'Payment option 1', 'required');
					if($this->form_validation->run())
					{

						 $data = array(
										'gift_id' => $this->input->post('gift_id'),
										'type' => $this->input->post('type'),
										'image'=>$new_name,
										'spend_policy' => $this->input->post('spend_policy'),
										'value' => $this->input->post('value'),
										'quantity' => $this->input->post('quantity'),
										'payment_opt1' => $this->input->post('payment_opt1'),
										'payment_opt2' => $this->input->post('payment_opt2'),
										"created" => date('Y-m-d H:i:s')
								 	 );

								// echo '<pre>';print_r($data); die();
                          if($id == 0){

								$this->db->insert('discount_coupons', $data);
								$this->session->set_flashdata('success', 'Coupon created successfully');
								redirect('coupons');
							}else{

									// $this->db->where('id', $id);
	        //                         $this->db->update('gift_card', $data);
									// $this->session->set_flashdata('success', 'gift card updated successfully');
									// redirect('/pages');
	                                }

							}
							else{

								$this->session->set_flashdata('error', 'Enter some values');
								redirect('/add-coupan');
							}


		}
	}


	    public function get_listcoupon(){

			if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{
					$this->load->view('admin/layouts/header');
					$this->load->view('admin/layouts/side-bar');
					$data['final']= $this->admin_model->get_coupon();
					$this->load->view('admin/list_coupons', $data);
					$this->load->view('admin/layouts/footer');

				}
         }

         public function delete_coupon($id){

			$this->admin_model->delete_coupon($id);
            $this->session->set_flashdata('success', 'coupon Deleted successfully');
			$data['final']= $this->admin_model->get_card();
			redirect('coupons');


		}



	  public function update_coupon_page($id)
		    {
			if(!$this->session->userdata('email')){
					redirect('/admin');
				}
				else
				{
								$this->load->view('admin/layouts/header');
								$this->load->view('admin/layouts/side-bar');
								$data['final']= $this->admin_model->get_card();
								$data['result']= $this->admin_model->fetch_get_coupon_by_id($id);
								$this->load->view('admin/update_coupon', $data);
								// $this->load->view('admin/add_pages');
								$this->load->view('admin/layouts/footer');
			       	}

	}


			public function update_coupon($id){

			if($_FILES["image"]['name']!=""){


      			$config['upload_path']          = './assets/admin/images/';
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|txt|csv|xml|mp3|mp4';
                $config['overwrite']            = TRUE;
                $config['max_size']             = 2048000; // Can be set to particular file size , here it is 2 MB(2048 Kb)
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $new_name = time().$_FILES["image"]['name'];
				$config['file_name'] = $new_name;

                $this->load->library('upload', $config);

              if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        // echo '<pre>';
                        // print_r($error);
                        // die;

                        // $this->load->view('manage_profile', $error);
                	   $this->session->set_flashdata('error', $error);
                }else{

                	$image_data = $this->upload->data();
                   // echo '<pre>';print_r($image_data); die();

					$this->load->library('form_validation');
					$this->form_validation->set_rules('gift_id', 'Gift card', 'required');
					$this->form_validation->set_rules('type', 'Type', 'required');
					$this->form_validation->set_rules('spend_policy', 'Spend policy', 'required');
					$this->form_validation->set_rules('value', 'Value', 'required');

					$this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric');
					$this->form_validation->set_rules('payment_opt1', 'Payment option 1', 'required');
					$this->form_validation->set_rules('payment_opt2', 'Payment option 1', 'required');
					if($this->form_validation->run())
					{

						 $data = array(
										'gift_id' => $this->input->post('gift_id'),
										'type' => $this->input->post('type'),
										'image'=>$new_name,
										'spend_policy' => $this->input->post('spend_policy'),
										'value' => $this->input->post('value'),
										'quantity' => $this->input->post('quantity'),
										'payment_opt1' => $this->input->post('payment_opt1'),
										'payment_opt2' => $this->input->post('payment_opt2'),
										"created" => date('Y-m-d H:i:s')
								 	 );

						  // echo '<pre>';print_r($data); die();

									$this->db->where('id', $id);
	                                $this->db->update('discount_coupons', $data);
									$this->session->set_flashdata('success', 'Coupon updated successfully');
									redirect('/coupons');

							}
							else{
								echo "error";
							}


						}

						}else{
			                $new_name=$this->input->post('old');
									 $data = array(
										'gift_id' => $this->input->post('gift_id'),
										'type' => $this->input->post('type'),
										'image'=>$new_name,
										'spend_policy' => $this->input->post('spend_policy'),
										'value' => $this->input->post('value'),
										'quantity' => $this->input->post('quantity'),
										'payment_opt1' => $this->input->post('payment_opt1'),
										'payment_opt2' => $this->input->post('payment_opt2'),
										"created" => date('Y-m-d H:i:s')
								 	 );

                                 // echo '<pre>';print_r($data); die();
									$this->db->where('id', $id);
	                                $this->db->update('discount_coupons', $data);
									$this->session->set_flashdata('success', 'Coupon updated successfully');
									redirect('/coupons');


		}

}

/*keyur shah code starts from here*/


	public function activate() {
		$user_id = $this->input->post( 'userid' );

		$data = array(
			'activated_at' => date('Y-m-d H:i:s'),
		);

		$this->db->where('id', $user_id);
		$this->db->update('clients_web', $data);

		$response = array();
		$response['success'] = true;
		$response['data'] = $user_id;
		echo json_encode($response);

	}

	public function deactivate() {
		$user_id = $this->input->post( 'userid' );

		$data = array(
			'activated_at' => NULL,
		);

		$this->db->where('id', $user_id);
		$this->db->update('clients_web', $data);

		$response = array();
		$response['success'] = true;
		$response['data'] = $user_id;
		echo json_encode($response);

	}



}
