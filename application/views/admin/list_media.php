<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/AdminLTE.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/skins/_all-skins.min.css'); ?>">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style>
.btn-group>.btn+.dropdown-toggle {
    padding-right: 5px;
    padding-left: 5px;
    height: 34px;
}

.btn {
    display: inline-block;
    padding: 6px 4px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}

</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">

  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          All Heading
          <!-- <small>advanced tables</small> -->
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Heading</th>
                    <th>Attachments</th>
                    <th>User Name</th>
                    <th>User Email</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($final as $list){?>
                  <tr>
                    <td><?php echo $list['heading'];?></td>
                    <td>
                      <?php foreach ($list['file_list'] as $attachment): ?>
                       <li>
                         <a download href='<?php $file_path  = './assets/user/images/' . $attachment['path']; echo base_url($file_path) ?> '>
                           <?php echo  $attachment['name'] ?>
                         </a>
                       </li>
                      <?php endforeach ?>
                    </td>
                    <td>
                      <?= $list['user_details']->firstname ?>
                    </td>
                    <td>
                      <?= $list['user_details']->email ?>
                    </td>
                  </tr>
                <?php }?>

                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> -->
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>

<script src="<?php echo base_url('/assets/bower_components/jquery/dist/jquery.min.js');?>"></script>

<script src="<?php echo base_url('/assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>

<script src="<?php echo base_url('/assets/bower_components/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>

<script src="<?php echo base_url('/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>

<script src="<?php echo base_url('/assets/bower_components/fastclick/lib/fastclick.js');?>"></script>

<script src="<?php echo base_url('/assets/js/adminlte.min.js');?>"></script>

<script src="<?php echo base_url('/assets/js/demo.js');?>"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

<script>
function doconfirm()
{
    job=confirm("Are you sure to Delete?");
    if(job!=true)
    {
        return false;
    }
}
</script>

</body>
</html>
