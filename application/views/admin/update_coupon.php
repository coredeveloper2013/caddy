<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/AdminLTE.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/skins/_all-skins.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update coupon
        <!-- <small>Preview</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update</h3>
                  <?php if ($this->session->flashdata('success')) { ?>
                  <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                  <?php } ?>

                 <?php if ($this->session->flashdata('error')) { ?>
                 <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
                  <?php } ?>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="form" action="<?php echo base_url('admin_controller/update_coupon/'.$result['id']);?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">

                <div class="form-group">
                  <label>Select giftcard</label>
                  <select class="form-control" name="gift_id">
                    <option value="">Please Select Option</option>
                    <?php foreach($final as $select){ ?>
                    <option value="<?php echo $select['id'];?>" <?php if($select['id'] == $result['gift_id'] ){
                      echo "selected";
                    } ?> ><?php echo $select['title'];?></option>
                    <?php } ?>
                  </select>
                </div>

                 <div class="form-group">
                  <label for="exampleInputEmail1">Coupon type</label>
                  <input type="text" name="type" class="form-control" id="type" value="<?php if ($this->session->flashdata('error')){ echo $this->session->userdata('type');}elseif(isset($result)){ echo $result['type'];}?>" placeholder="Enter a coupon type ">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Spend policy</label>
                  <input type="text" name="spend_policy" class="form-control" value="<?php if ($this->session->flashdata('error')){ echo $this->session->userdata('spend_policy');}elseif(isset($result)){ echo $result['spend_policy'];}?>" id="spend_policy" placeholder="Enter a coupon Spend policy">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Value</label>
                  <input type="text" name="value" class="form-control" value="<?php if ($this->session->flashdata('error')){ echo $this->session->userdata('value');}elseif(isset($result)){ echo $result['value'];}?>" id="value" placeholder="Enter a coupon Value">
                </div>

                 <div class="form-group">
                  <label for="exampleInputEmail1">Quantity</label>
                  <input type="text" name="quantity" class="form-control" value="<?php if ($this->session->flashdata('error')){ echo $this->session->userdata('quantity');}elseif(isset($result)){ echo $result['quantity'];}?>" id="quantity" placeholder="Enter a coupon quantity">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Payment option 1</label>
                  <input type="text" name="payment_opt1" class="form-control" value="<?php if ($this->session->flashdata('error')){ echo $this->session->userdata('payment_opt1');}elseif(isset($result)){ echo $result['payment_opt1'];}?>" id="payment_opt1" placeholder="Enter a payment option">
                </div>

               <div class="form-group">
                  <label for="exampleInputEmail1">Payment option 2</label>
                  <input type="text" name="payment_opt2" class="form-control" value="<?php if ($this->session->flashdata('error')){ echo $this->session->userdata('payment_opt2');}elseif(isset($result)){ echo $result['payment_opt2'];}?>" id="payment_opt2" placeholder="Enter a payment option">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Image</label>
                  <input id="image" name="image" class="form-control" value= "<?php echo $result['image'];?>" placeholder="" value="" type="file">
                  <img src="<?php echo base_url() .'assets/admin/images/'.$result['image']; ?>" style="width:150px; height:130px;">
                  <input type="hidden"  id="old"  name="old"  value="<?php echo $result['image'];?>">
                </div>

<!--                 <div class="form-group">
                  <label for="exampleInputPassword1">Description</label>
                  <textarea id="editor1" name="content" rows="10" cols="80"><?php// if ($this->session->flashdata('error')){//  echo $this->session->userdata('content'); }elseif(isset($result)){ echo $result['content'];}?></textarea>
                </div> -->

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
          <!-- /.box -->
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script src="<?php echo base_url('/assets/bower_components/jquery/dist/jquery.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/fastclick/lib/fastclick.js');?>"></script>

  <script src="<?php echo base_url('/assets/js/adminlte.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/js/demo.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/ckeditor/ckeditor.js');?>"></script>

  <script src="<?php echo base_url('/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>

  <script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>

<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>

// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $('#form').validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      title: {
        required: true,
      },
      discount_per: {
        required: true,
      }
    },
    // Specify validation error messages
    messages: {

      title: {
        required: "Please provide a page title",
      },
      discount_per: {
        required: "Please provide a Discount",
      },
      // email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});



</script>

  </body>
  </html>
