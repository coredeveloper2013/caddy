<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('/assets/css/AdminLTE.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/css/skins/_all-skins.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<style>
label.error {
    color: #dd4b39;
}
</style>

</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Giftcard coupons
        <!-- <small>Preview</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('index.php/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New coupon</h3>
                          <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
                <?php } ?>

                <?php if ($this->session->flashdata('error')) { ?>
                <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
            <?php } ?>
            </div>
            <!-- /.box-header -->
            <!-- form start -->


            <form role="form" id="form" action="<?php echo base_url('admin_controller/add_coupon');?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">

                <div class="form-group">
                  <label>Select giftcard</label>
                  <select class="form-control" name="gift_id">
                    <option value="0">Please Select Option</option>
                    <?php foreach($final as $select){ ?>
                    <option value="<?php echo $select['id'];?>"><?php echo $select['title'];?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Coupon type</label>
                  <input type="text" name="type" class="form-control" id="type" placeholder="Enter a coupon type ">
                </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">Spend policy</label>
                  <input type="text" name="spend_policy" class="form-control" id="spend_policy" placeholder="Enter a coupon Spend policy">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Value</label>
                  <input type="text" name="value" class="form-control" id="value" placeholder="Enter a coupon Value">
                </div>

                 <div class="form-group">
                  <label for="exampleInputEmail1">Quantity</label>
                  <input type="text" name="quantity" class="form-control" id="quantity" placeholder="Enter a coupon quantity">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Payment option 1</label>
                  <input type="text" name="payment_opt1" class="form-control" id="payment_opt1" placeholder="Enter a payment option">
                </div>

               <div class="form-group">
                  <label for="exampleInputEmail1">Payment option 2</label>
                  <input type="text" name="payment_opt2" class="form-control" id="payment_opt2" placeholder="Enter a payment option">
                </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">Image</label>
                  <input id="image" name="image" class="form-control" placeholder="" type="file">
                </div>



              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
          <!-- /.box -->
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script src="<?php echo base_url('/assets/bower_components/jquery/dist/jquery.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/fastclick/lib/fastclick.js');?>"></script>

  <script src="<?php echo base_url('/assets/js/adminlte.min.js');?>"></script>

  <script src="<?php echo base_url('/assets/js/demo.js');?>"></script>

  <script src="<?php echo base_url('/assets/bower_components/ckeditor/ckeditor.js');?>"></script>

  <script src="<?php echo base_url('/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>


<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>

// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $('#form').validate({
    // Specify validation rules
    rules: {

     type: {
        required: true,
      },

      spend_policy: {
        required: true,
      },

      value: {
        required: true,
      },

     quantity: {
        required: true,
        number : true
      },
      payment_opt1: {
        required: true,
      },
       payment_opt2: {
        required: true,
      }
    },
    // Specify validation error messages
    messages: {

      type: {
        required: "Please provide a  Coupan type",
      },
      spend_policy: {
        required: "Please provide a  spend policy",
      },
      value: {
        required: "Please provide a coupon value",
      },
      quantity: {
        required: "Please provide a coupon value",
        number : "only number required"
      },
      payment_opt1: {
        required: "Please provide a payment option",
      },
      payment_opt2: {
        required: "Please provide a payment option",
      }
      // email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});



</script>

  </body>
  </html>
