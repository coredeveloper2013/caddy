<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Important Owl stylesheet -->
<!-- <link rel="stylesheet" href="<?php/// echo base_url('/assets/site/css/owl.carousel.css'); ?>"> -->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="<?php// echo base_url('/assets/site/css/owl.theme.default.min.css'); ?>"> -->

 <!-- <link rel="stylesheet" type="text/css" href="<?php///echo base_url('/assets/site/css/style.css'); ?>"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

<div class="site-section">
  <div class="container">
    <?php if ($this->session->flashdata('success')) { ?>
<div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
<?php } ?>

    <?php if ($this->session->flashdata('error')) { ?>
<div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
<?php } ?>
    <div  class="login-container">

      <div class="block push-bit">

      <form action="<?php echo base_url('/front/user_login'); ?>" method="post" id="form" class="form-vertical" style="display: block;">
      <div class="title-bordered">Change password</div>
      <div class="form-group">
      <label class="control-label" aria-required="true">New password</label> 
      <input id="password" name="password" class="form-control" placeholder="New password" type="Password">
      </div>
      <div class="form-group">
      <label class="control-label" aria-required="true">Confirm-password</label>
      <input id="confirm_password" name="confirm_password" class="form-control" placeholder="confirm password"  type="Password">
      </div>
      <div class="form-group form-actions">
      <button type="submit" class="btn btn-wide btn-lg btn-success">Login</button>
      </div>
      <div class="form-group text-center">
      <!-- <a href="#reminder" id="link-reminder-login">Forgot password?</a> - -->
      <a href="<?php echo base_url('sign-up')?>" id="link-register-login">Create a new account</a>
      </div>
      </form>

      </div>

    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>

// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $('#form').validate({
    // Specify validation rules
    rules: {

      password: {
        required: true,
        minlength: 8
      },
      confirm_password: {
        required: true,
        equalTo : "#password"
      }
    },
    // Specify validation error messages
    messages: {

      password:{
       required: "Please provide a new password",
       minlength: "Your password must be at least 8 characters long"
     },

       confirm_password:{
        required: "Please provide a cofirm-password",
        equalTo: "password must be same as new password"
      },
    },

    submitHandler: function(form) {
      form.submit();
    }
  });
});
  </script>
</body>
</html>
