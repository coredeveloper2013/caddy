<div class=footer>
  <div id="shopify-section-footer" class="shopify-section">
  <div class="footer-top-section section pt-100 ph4">
    <div class="container">
      <div class="row">
        <div class="footer-widget col-md-4 col-sm-6 col-xs-12 mb-40">
          <h4 class="widget-title">
            <a href="<?php echo base_url('/');?>" class="white-logo">
              <img src="<?php echo base_url('assets/site/images/logo_white.png');?>" width="150" alt="logo">
            </a>
          </h4>
<!--           <p class="mb2">
            <strong class="pr3">Address:</strong>
            <span>51  Terrick Rd A<br> EGTON DE YO21 7DY</span>
          </p> -->
          <p class="mb2">
            <strong class="pr3">Email:</strong>
            <span>support@cardcaddy.co</span>
          </p>
          <!-- <p class="mb2">
            <strong class="pr3">Chat</strong>
            <a href="#shopify-section-footer" id="footer-chat" class="link">Start Chat</a>
          </p> -->
        </div>

        <div class="footer-widget link-widget col-md-4 col-sm-6 col-xs-12 mb-40">
          <h4 class="widget-title">
            Directory
          </h4>
          <ul>

            	<li>
                <a class="fot-a" href="<?php echo base_url('about-us')?>">About Us</a>
            	</li>

            	<li>
                <a class="fot-a" href="<?php echo base_url('contact-us')?>">Contact Us</a>
            	</li>
            	<li>
                <a class="fot-a" href="<?php echo base_url('pages/terms-of-use')?>">Terms of Use</a>
            	</li>
                <li>
                <a class="fot-a" href="<?php echo base_url('pages/privacy-policy')?>">Privacy Policy</a>
            	</li>

            	<li>
                <a class="fot-a" href="<?php echo base_url('pages/spend-policy')?>">Spend Policy</a>
            	</li>

            	<li>
                <a class="fot-a" href="<?php echo base_url('pages/refund-policy')?>">Refund Policy</a>
            	</li>




          </ul>
        </div>

        <div class="footer-widget col-md-4 col-sm-6 col-xs-12 mb-40">

          <h4 class="widget-title">
            Community
          </h4>

          <p class="widget-p">Get notified instantly in Telegram as soon as new gift cards are added to the site. Hear about exclusive member offers, and more!</p>

          <a href="https://t.me/thecardcaddy" target="_blank" class="light-purple">
            <i class="fa fa-telegram"></i>Join us on Telegram
          </a> 

        </div>
      </div>
    </div>
  </div>

<!--   <div class="footer-middle-section section">
    <div class="container">
      <div class="row">
        <div class="db ph4" style="float: left;">
          <a href="#" class="dim">
            <img src="<?php //echo base_url('assets/site/images/btc_payment.png');?>" class="dib ph2-ns" alt="Bitcoin">
          </a>
          <a href="#" class="dim">
            <img src="<?php //echo base_url('assets/site/images/bch_payment.png');?>" class="dib ph2-ns" alt="Payment">
          </a>
          <a href="#" target="_blank" class="dim">
            <img src="<?php //echo base_url('assets/site/images/trustpilot.png');?>" class="dib pt3 pt0-ns ph3-ns" alt="TrustPilot">
          </a>
        </div>
      </div>
    </div>
  </div> -->
<!-- 
<div class="main">
 

</div> -->
  <!-- FOOTER-SUB SECTION START -->

  <div class="footer-bottom-section section pv5">
    <div class="container">
      <div class="row">
        <div class="copyright ph4">
          <p class="gray f4 ma2 lh-title">© 2018, CardCaddy. All Rights Reserved.</p>
          <p class="gray f4 ma2 lh-title">CardCaddy is <strong>not</strong> an official partner of Amazon.com, Inc, Home Depot, Walmart, Target or any of their affiliates.</p>
          <p class="mid-gray f4 ma2 lh-title">
             <a href="<?php echo base_url('pages/terms-of-use')?>" class="o-70">Terms</a> • <a href="<?php echo base_url('pages/privacy-policy')?>" class="o-70">Privacy</a>
          </p>
        </div>
      </div>
    </div>
  </div>

  <!-- FOOTER-SUB SECTION END -->

  </div>
</div>

<script>
jQuery(document).ready(function() {
     jQuery('.toggle').click(function(e){
           e.preventDefault();
             jQuery('#main-menu').slideToggle('slow');
       });
});
</script>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>

