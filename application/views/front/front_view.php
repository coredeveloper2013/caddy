<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('/assets/site/css/style.css'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<div class="in-card">
  <div class="container">
    <h3 class="site-heading text-center">Top Selling Cards</h3>
    <div class="row">
      <div class="col-md-12 floatnone-center">
        <div class="row">
          <?php foreach($final as $list){?>
            <div class="col-xs-12 col-md-3" itemscope="" itemtype="http://schema.org/Product">
            <a itemprop="url" href="<?php 

             $title = $list['title'];
             $replace = str_replace(' ', '-', $title);
             echo base_url('discount/'.strtolower($replace))?>" 

             class="lnk-giftcard">
            <img itemprop="image" class="cart-image" src="<?php echo base_url() .'assets/admin/images/'.$list['image']; ?>">
            <span class="desc">
            <span itemprop="brand" itemscope="" itemtype="http://schema.org/Brand">
            <span itemprop="name"><?php echo $list['title'];?></span>
            </span>
            <span class="hidden" itemprop="name">Walmart Gift Cards</span>
            <span class="light">Save up to <?php echo $list['discount_per'];?>%</span>
            </span>
            </a>
          </div>
         <?php  }?>
      </div>
      </div>

      <div id="shopify-section-1516532685515" class="shopify-section Immediate-section"><hr class="pb5  ">

<div class="w-100 mb3">
  <div class="container">
    <div class="mw8 center tc">
      <h2 class="fw5">Immediate Delivery</h2>
      <p class="silver mw8 pt2">Claim and spend your gift cards in minutes.</p>
    </div>
  </div>
</div>

<div class="w-100 mb6">
  <div class="container">
    <div class="row">
      <div class="fl w-100 w-third-ns pa2 ph3-ns">
        <div class="ph3 tc">
          <img src="<?php echo base_url('/assets/site/images/step-01.png'); ?>" alt="">
        </div>
      </div>

      <div class="fl w-100 w-third-ns pa2 ph3-ns">
        <div class="ph3 tc">
          <img src="<?php echo base_url('/assets/site/images/step-02.png'); ?>" alt="">
        </div>
      </div>

      <div class="fl w-100 w-third-ns pa2 ph3-ns">
        <div class="ph3 tc">
          <img src="<?php echo base_url('/assets/site/images/step-03.png'); ?>" alt="">
        </div>
      </div>
      
    </div>
  </div>
</div>






</div>
    </div>


  </div>
</div>

<div class="lower-card">
  <div class="container">
<!--   <div class="spend-policy-desc">
  <h2>
  Spend gift cards faster, <br><span class="hidden-xs ml-120"></span>Earn greater discounts!
  </h2>
  <h4>
  Spend cards faster, Save more
  </h4>
  </div> -->
  <!-- <img class="spend-policy-banner" src="<?php //echo base_url('assets/site/images/spend-polic.png');?>" alt="gift cards spend policy"> -->
  </div>
</div>
      

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
</body>
</html>
