<?php

class User_model extends CI_Model
  {

    public function insert_users($data){

        $this->db->insert('clients_web', $data);
        return true;

    }
    public function attachments_by_clients_web_id($clients_web_id)
    {
        $where['clients_web_id'] = $clients_web_id;
       $result_set = $this->db->get_where('attachments', $where);
       return $result_set->result_array();
    }
    public function delete_attachment($id, $clients_web_id) {
      $this->db->delete('attachments', array('id' => $id));
      return $this->attachments_by_clients_web_id($clients_web_id);
    }
    public function insert_attachment($name, $path, $clients_web_id)
    {
        $data =[
          'name' => $name,
          'path' => $path,
          'clients_web_id' => $clients_web_id
        ];

        $this->db->insert('attachments', $data);
        $id = $this->db->insert_id();        // return $this->db->
        $this->db->select( '*' );
        $this->db->where( 'id',  $id);
        $q = $this->db->get( 'attachments' );
        $first = $q->row();
        return $first;
        // return $this->attachments_by_clients_web_id($clients_web_id);
    }

    public function insert_contact($data){

        $this->db->insert('contact_us', $data);
        return true;

    }

    public function can_login($email, $password)
    {
         $this->db->where('email', $email);
         $this->db->where('password', $password);
         $query = $this->db->get('clients_web');
         //SELECT * FROM users WHERE username = '$username' AND password = '$password'
         if($query->num_rows() > 0)
         {
              return true;
         }
         else
         {
              return false;
         }
    }

    public function getall($email)
     {
      $this->db->select('*');
      $this->db->where("email",$email);
      $q=$this->db->get('clients_web');
      return $q->row();
    }

    public function about_us(){

      $this->db->select('*');
      $this->db->where('slug','about-us');
      $q=$this->db->get('pages');
      $data= $q->row();
      echo "<pre>"; print_r($data); die();
    }


   public function get_card(){
     $this->db->select('*');
     $this->db->from('gift_card');
     $this->db->order_by("id", "asc");
     $query=$this->db->get();
     return $final = $query->result_array();
    }


    public function users(){

     $this->db->select('*');
     $this->db->from('clients_web');
     $query=$this->db->get();
     return  $query->result_array();
     //  echo '<pre>'; print_r($final_data); die();

    }

       public function ForgotPassword($email){
       $this->db->select('email');
       $this->db->from('clients_web');
       $this->db->where('email', $email);
       $query=$this->db->get();
        return $query->row_array();

          //   echo '<pre>';
          // print_r($data); die();
     }



  }
?>
