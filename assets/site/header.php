
<!-- Header -->
<div class="header">
  <header class="header-section section sticker header-transparent for-notification-bar">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 col-sm-6 col-xs-6 header-left">
          <div class="header-logo">

            <a href="<?php echo base_url('/');?>" class="white-logo">
              <!-- <img src="<?php// echo base_url('assets/site/images/logo.png');?>" alt="logo"> -->
              <img src="<?php echo base_url('assets/site/images/logo_white.png');?>" alt="logo">
            </a>
            <!-- <a href="/" class="dark-logo">
              <img src="//storage.googleapis.com/redeeem/assets/images/logo_regular.png" alt="logo">
            </a> -->

          </div>
		  <a class="toggle" href="javascript:void(0)"><span class="toggle-line"></span><span class="toggle-line"></span><span class="toggle-line"></span></span></a>

        </div>




         <!-- DROPDOWN MENU -->
        <!-- <div class="col-md-2 col-sm-6 col-xs-6 float-right">
          <div class="header-option-btns float-right">



            <div class="header-account float-left">
              <ul>
                <li class="">

                  <a href="#" data-toggle="dropdown" aria-expanded="false">
                    <i class="pe-7s-config"></i>
                  </a>

  				<ul class="dropdown-menu">

                    <li>
                    	<a href="/pages/about">
                        About
                      </a>
                    </li>

                    <li>
                    	<a href="/collections/amazon-gift-cards?sort_by=created-ascending">
                        Offers
                      </a>
                    </li>

                    <li>
                    	<a href="/pages/guarantee">
                        Guarantee
                      </a>
                    </li>

                    <li>
                    	<a href="/products/redeeem-gift-card">
                        Gift Cards
                      </a>
                    </li>

                    <li>
                    	<a href="/pages/bitpay">
                        BitPay
                      </a>
                    </li>






                    <li>
                        <a href="/pages/kyc">
                          KYC Process
                        </a>
                    </li>







                      <li>
                          <a href="#">
                            Signup
                        	</a>
                      </li>
                      <li>
                          <a href="/account/login">
                            Login
                        	</a>
                      </li>


                  </ul>
                </li>
              </ul>
            </div>

  <div class="header-cart float-left">

    <a class="cart-toggle" href="#" data-toggle="dropdown">
      <i class="pe-7s-cart"></i>
      <span class="bigcounter">0</span>
    </a>

    <div class="mini-cart-brief dropdown-menu text-left">

      <div class="w-100 mt3">
        <p class="f3 tc">

          	You have <span class="fw7">0</span> items in your cart.

        </p>
      </div>

      <hr class="mt4 mb0">

      <div class="all-cart-product mt4 clearfix">
        <ul>

        </ul>
      </div>

      <div class="h2 ph4">
        <span class="fl tc f3 ph4">Total</span>
        <span class="fr tc f3 ph4 success">$0.00</span>
      </div>
      <hr class="pb2">

      <div class="cart-bottom w-60 clearfix center">
        <a href="/cart">Go to Cart</a>
      </div>

      <a href="/cart/clear" class="clear-cart dark-gray mt4 mb3 f4 tc db lh-solid">Empty Cart</a>

    </div>
  </div>
          </div>
        </div> 
 -->


        <!-- primary-menu -->
        <div class="col-md-8 col-xs-12 header-right">
          <nav class="main-menu text-center" id="main-menu">
    <ul>
       <?php if($this->session->userdata('email') && !empty($this->session->userdata('email')) )
            {
            ?>
        <li><a href="<?php echo base_url('giftcard')?>">BUY GIFT CARDS</a></li>
        <?php }else{ ?>
                 <li><a href="<?php echo base_url('login')?>">BUY GIFT CARDS</a></li>
        <?php } ?>
        <li><a href="<?php echo base_url('about-us')?>">About us</a></li>
        <li><a href="<?php echo base_url('contact-us')?>">contact us</a></li>
        <?php if($this->session->userdata('email') && !empty($this->session->userdata('email')) )
            {
            ?>
                  <li><a href="<?php echo base_url('account')?>">Account</a></li>
                  <li><a href="<?php echo base_url('logout')?>">Logout</a></li>
            <?php }else{ ?>

              <li><a href="<?php echo base_url('sign-up')?>">Signup</a></li>
            	<li><a href="<?php echo base_url('login')?>">Login</a></li>

            <?php } ?>


    </ul>
  </nav>
          <div class="mobile-menu"></div>
        </div>
      </div>
    </div>
  </header>
</div>
<!-- header-end -->

<main role="main">
		<!-- BEGIN content_for_index -->
  <div id="shopify-section-1516534241377" class="shopify-section">
      <div class="home-slider-section">

     <div id="home-slider" class="cover">

       <div class="hero-slider-content tc pt7">

         <h1 class="white fw7 ma0 lh-title">
           Gift Cards Buying &amp; Selling Made Easy
         </h1>

         <p class="near-white f2-ns f3 pa3 ma0 lh-title">
           Buy Discounted Gift Cards<br>on CardCaddy.
         </p>
         <p class="near-white f2-ns f3 pa3 ma0 lh-title">
           Greater discounts, super easy to use, most popular brands
         </p>


           <a href="<?php echo base_url('sign-up')?>" class="hero-call-to-action">
             <i class="fal fa-globe-africa pr2"></i>
             Get Started
           </a>

       </div>
           </div>
         </div>
     </div>

	</main>
<!-- END content_for_index -->
